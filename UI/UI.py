from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.graphics import Color, Rectangle, Line
from kivy.uix.scrollview import ScrollView
import kivy.utils as utils
from kivy.uix.textinput import TextInput
from kivy.properties import NumericProperty





class Texte(Label):
    def __init__(self, **kwargs):
        Label.__init__(self, **kwargs)
        self.bind(size=self.set_size)

    def set_size(self, widget, value):
        widget.text_size = self.size


class Bouton(Button):
    def __init__(self, color="red", **kwargs):
        Button.__init__(self, color=color, **kwargs)


class TotalLabel(Label):
    def on_size(self, *args):
        self.canvas.before.clear()
        with self.canvas.before:
            Color(0, 1, 0, 0.25)
            Rectangle(size=self.size, pos_hint={'x': 0.5, 'y': 0.8}, size_hint=self.size_hint)

class BackgroundCustomerManagement(Label):
    def on_size(self, *args):
        self.canvas.before.clear()
        with self.canvas.before:
            Color(rgb=utils.get_color_from_hex('#ff5f99'))
            Rectangle(size=self.size, pos_hint={'center_x': 0.5, 'center_y': 0.5}, size_hint=self.size_hint)
            # Line(width=3, rectangle=(self.x, self.y, self.width, self.height))

class BackgroundManagementSaleBorrowing(Label):
    def on_size(self, *args):
        self.canvas.before.clear()
        with self.canvas.before:
            Color(rgb=utils.get_color_from_hex('#ffff99'))
            Rectangle(size=self.size, pos_hint={'center_x': 0.5, 'center_y': 0.5}, size_hint=self.size_hint)

class BackgroundRoleManagement(Label):
    def on_size(self, *args):
        self.canvas.before.clear()
        with self.canvas.before:
            Color(rgb=utils.get_color_from_hex('#26a6ed'))
            Rectangle(size=self.size, pos_hint={'center_x': 0.5, 'center_y': 0.5}, size_hint=self.size_hint)
            # Line(width=3, rectangle=(self.x, self.y, self.width, self.height))

class MyLabel(Label):
    def on_size(self, *args):
        self.text_size = self.size


class MyButton(Button):
    def on_size(self, *args):
        self.text_size = self.size


class CustomerButton(Button):
    def __init__(self, text, customer, **kwargs):
        Button.__init__(self, **kwargs)
        self.text = text
        self.customer = customer


class PageIDButton(Button):
    def __init__(self, pageID, **kwargs):
        Button.__init__(self, **kwargs)
        self.pageID = pageID


class EmployeeButton(Button):
    def __init__(self, text, employee, **kwargs):
        Button.__init__(self, **kwargs)
        self.text = text
        self.employee = employee

class BookToggleButton(ToggleButton):
    def __init__(self, book, **kwargs):
        ToggleButton.__init__(self, **kwargs)
        self.book = book

class CustomerHistoryToggleButton(ToggleButton):
    def __init__(self, transactions_history, **kwargs):
        ToggleButton.__init__(self, **kwargs)
        self.transactions_history = transactions_history


class CustomerToggleButton(ToggleButton):
    def __init__(self, text, customer, **kwargs):
        ToggleButton.__init__(self, **kwargs)
        self.text = text
        self.customer = customer

class MyTextInput(TextInput):
    max_characters = NumericProperty(0)
    def insert_text(self, substring, from_undo=False):
        if len(self.text) > self.max_characters and self.max_characters > 0:
            substring = ""
        TextInput.insert_text(self, substring, from_undo)



