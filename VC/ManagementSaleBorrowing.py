from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.core.window import Window
from Model.Graphs import BaseGraphBorrow
from Model.Graphs import BaseGraphSales
from kivy.uix.popup import Popup

from Model.list import ListObject

from UI.UI import *

import re


class ManagementSaleBorrowing(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller
        self.controller.load_customers()
        self.controller.load_book()
        self.controller.load_type()
        self.controller.load_categories()
        self.customer_press = []
        self.book_press = []
        self.author_press = []
        self.type_press = []
        self.category_press = []

        ########################
        #      BACKGROUND      #
        ########################

        self.menu_background = BackgroundManagementSaleBorrowing(text="", size_hint=(1,1))

        ########################
        #        FOOTER        #
        ########################

        # ---Boutton Retour + Bind de Clbk_previous ( retour au menu )
        self.buttonreturn = Button(text='Retour', size_hint=(0.15, 0.07), background_normal = "",
        background_color = utils.get_color_from_hex('#ffb266'))

        self.buttonreturn.pos_hint = {'x': 0.035, 'y': 0.05}
        self.buttonreturn.bind(on_release=self.clbk_previous)

        # --- Boutton Validé ---
        self.buttonconfirm = Button(text='Validé', size_hint=(0.15, 0.07), background_normal = "",
        background_color = utils.get_color_from_hex('#ffb266'))
        self.buttonconfirm.bind(on_release=self.show_graph)
        self.buttonconfirm.pos_hint = {'x': 0.835, 'y': 0.05}

        # --- Boutton Ventes---
        self.buttonsell = ToggleButton(text='Ventes', size_hint=(0.15, 0.05))
        self.buttonsell.pos_hint = {'x': 0.19, 'y': 0.05}

        # ---Boutton Emprunts---
        self.buttonnborrowing = ToggleButton(text='Emprunts', size_hint=(0.15, 0.05))
        self.buttonnborrowing.pos_hint = {'x': 0.680, 'y': 0.05}


        ########################
        #        LIVRE         #
        ########################

        # --- Boutton LIVRE + Input Recherche---
        self.labelbook = Button(text='Livre', disabled=True,background_color=( 0.0, 0.0, 0.0, 1.0 ), size_hint=(0.20, 0.05))
        self.labelbook.pos_hint = {'center_x': 0.5, 'center_y': 0.93}
        # --- TextInput Recherche LIVRE +
        self.inputbook = TextInput(multiline=False,write_tab=False, size_hint=(0.20, 0.04))
        self.inputbook.pos_hint = {'center_x': 0.5, 'center_y': 0.89}
        self.inputbook.bind(text=self.input_research_book)

        # ---Création d'un GridLayout de LIVRE + Widget de list de LIVRE---
        self.scroll_layout = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout.bind(minimum_height=self.scroll_layout.setter('height'))
        # ---Button de scroll view---
        self.scroll_view = ScrollView(size_hint=(0.2, .2), size=(Window.width, Window.height),
                                      pos_hint={'x': .4, 'y': .67})
        self.scroll_view.add_widget(self.scroll_layout)
        self.list_book_button()

        ########################
        #        GENRE         #
        ########################

        # ---Boutton Genre---
        self.buttoncategory = Button(text='Genre', disabled=True,background_color=( 0.0, 0.0, 0.0, 1.0 ), size_hint=(0.2, 0.05))
        self.buttoncategory.pos_hint = {'x': 0.08, 'y': 0.46}

        # ---Création d'un GridLayout de GENRE + Widget de list de Genre---
        self.scroll_layout_category = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout_category.bind(minimum_height=self.scroll_layout_category.setter('height'))
        #---Button avec scroll view---
        self.scroll_category = ScrollView(size_hint=(0.2, .2), size=(Window.width, Window.height),
                                          pos_hint={'x': .08, 'y': .26})
        self.scroll_category.add_widget(self.scroll_layout_category)
        self.list_genre_button()


        ########################
        #        CLIENT        #
        ########################

        # --- Boutton Client + Input Recherche ---
        self.labelcustomer = Button(text='Client', disabled=True,background_color=( 0.0, 0.0, 0.0, 1.0 ), size_hint=(0.20, 0.05))
        self.labelcustomer.pos_hint = {'right': 0.9, 'top': 0.96}
        # --- TextInput Recherche de Client + Bind de l'Input_research ---
        self.inputcustomer = TextInput(multiline=False,write_tab=False, size_hint=(0.20, 0.04))
        self.inputcustomer.pos_hint = {'right': 0.9, 'top': 0.92}
        self.inputcustomer.bind(text=self.input_research)

        # ---Scroll_layout_customer + Widget de list de CLIENT---
        self.scroll_layout_customer = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout_customer.bind(minimum_height=self.scroll_layout_customer.setter('height'))
        # ---Button in scroll view---
        self.scroll_customer = ScrollView(size_hint=(0.2, .3), size=(Window.width, Window.height),
                                          pos_hint={'x': .7, 'y': .58})
        self.scroll_customer.add_widget(self.scroll_layout_customer)
        self.list_customer_button()


        ########################
        #        AUTEUR        #
        ########################

        # --- Boutton AUTEUR + Input Recherche---
        self.labelauthor = Button(text='Auteur', disabled=True,background_color=( 0.0, 0.0, 0.0, 1.0 ), size_hint=(0.20, 0.05))
        self.labelauthor.pos_hint = {'right': 0.6, 'top': 0.62}
        # --- TextInput Recherche AUTEUR +
        self.inputauthor = TextInput(multiline=False,write_tab=False, size_hint=(0.20, 0.04))
        self.inputauthor.pos_hint = {'right': 0.6, 'top': 0.58}
        self.inputauthor.bind(text=self.input_research_author)

        # ---Scroll_layout_author + Widget de list de AUTHOR---
        self.scroll_layout_author = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout_author.bind(minimum_height=self.scroll_layout_author.setter('height'))
        # ---Button in Scroll View---
        self.scroll_author = ScrollView(size_hint=(0.2, .2), size=(Window.width, Window.height),
                                        pos_hint={'x': .4, 'y': .34})
        self.scroll_author.add_widget(self.scroll_layout_author)
        self.list_book_author_button()


        ########################
        #        TYPE          #
        ########################

        # ---Button + Input TYPE---
        self.buttontype = Button(text='Type', disabled=True,background_color=( 0.0, 0.0, 0.0, 1.0 ), size_hint=(0.2, 0.05))
        self.buttontype.pos_hint = {'x': 0.7, 'y': 0.44}

        # ---Création d'un GridLayout de TYPE + Widget de list de TYPE---
        self.scroll_layout_type = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout_type.bind(minimum_height=self.scroll_layout_type.setter('height'))
        # ---Button avec ScrollView---
        self.list_type_button()
        self.scroll_type = ScrollView(size_hint=(0.2, .2), size=(Window.width, Window.height),
                                      pos_hint={'x': 0.7, 'y': 0.24})
        self.scroll_type.add_widget(self.scroll_layout_type)


        ########################
        #     CALENDRIER       #
        ########################

        # --- Boutton Période---
        self.labelperiod = Button(text='Veuillez entrer la période souhaiter',background_color=( 0.0, 0.0, 0.0, 1.0 ), disabled=True, size_hint=(0.3, 0.05))
        self.labelperiod.pos_hint = {'x': 0.05, 'y': 0.91}

        self.labelstart = Button(text='Entrer la valeur de Départ', disabled=True,background_color=( 0.0, 0.0, 0.0, 1.0 ), size_hint=(0.14, 0.04))
        self.labelstart.pos_hint = {'right': 0.19, 'top':0.89}
        self.date_start = MyTextInput(multiline=False,write_tab=False,max_characters=9, size_hint=(0.046,0.04))
        self.date_start.pos_hint = {'right': 0.145, 'top':0.85}

        self.bouttonsl = Button(text='/', background_color=(0.0, 0.0, 0.0, 1.0), disabled=True, size_hint=(0.02,0.04))
        self.bouttonsl.pos_hint = {'right': 0.21, 'top':0.85}

        self.labelend =  Button(text='Entrer la valeur de Fin', disabled=True,background_color=( 0.0, 0.0, 0.0, 1.0 ), size_hint=(0.14, 0.04))
        self.labelend.pos_hint = {'right': 0.35, 'top':0.89}
        self.date_end = MyTextInput(multiline=False,write_tab=False,text='01/01/2001',max_characters=9,tab_width=4, size_hint=(0.046,0.04))
        self.date_end.pos_hint = {'right':0.3, 'top':0.85}

        #--- appel de toute les functions ---
        self.layout()

    # --- mes AddWidget ---
    def layout(self):
        self.add_widget(self.menu_background)
        self.add_widget(self.buttonreturn)
        self.add_widget(self.buttonconfirm)
        self.add_widget(self.labelperiod)
        self.add_widget(self.labelbook)
        self.add_widget(self.labelcustomer)
        self.add_widget(self.inputbook)
        self.add_widget(self.inputcustomer)
        self.add_widget(self.buttonsell)
        self.add_widget(self.buttonnborrowing)
        self.add_widget(self.scroll_view)
        self.add_widget(self.scroll_customer)
        self.add_widget(self.scroll_author)
        self.add_widget(self.labelauthor)
        self.add_widget(self.inputauthor)
        self.add_widget(self.buttoncategory)
        self.add_widget(self.scroll_category)
        self.add_widget(self.buttontype)
        self.add_widget(self.scroll_type)
        self.add_widget(self.date_start)
        self.add_widget(self.date_end)
        self.add_widget(self.labelstart)
        self.add_widget(self.labelend)
        self.add_widget(self.bouttonsl)

    # --- List Boutton de nom de livre ---
    def list_book_button(self):
        for book in self.controller.list_books:
            button_text = book.name
            btn = BookToggleButton(text=button_text, book=book, size_hint_y=None, height=30)
            btn.bind(on_release=self.update_toggle_book)
            self.scroll_layout.add_widget(btn)

    #--- list Boutton d'autheur de livre---
    def list_book_author_button(self):
        list = ListObject()
        list_author = list.load_list_of_author()
        for author in list_author:
            button_text = author
            btn = BookToggleButton(text=button_text, book=author, size_hint_y=None, height=30)
            btn.bind(on_release=self.update_toggle_author)
            self.scroll_layout_author.add_widget(btn)

    #--- list Boutton type de livre
    def list_type_button(self):
        list = ListObject()
        list_type = list.load_list_of_author()
        for book in self.controller.list_type:
            button_text = book.type
            btn = BookToggleButton(text=button_text, book=book, size_hint_y=None, height=30)
            btn.bind(on_release=self.update_toggle_type)
            self.scroll_layout_type.add_widget(btn)

    # --- List de CLIENT ---
    def list_customer_button(self):
        for item in self.controller.list_customers:
            button_text = item.firstname + " " + item.lastname
            btn = CustomerToggleButton(text=button_text, customer=item, size_hint_y=None, height=30)
            btn.bind(on_release=self.update_toggle_customer)
            self.scroll_layout_customer.add_widget(btn)

    # --- List de GENRE ---
    def list_genre_button(self):
        for item in self.controller.list_categories:
            button_text = item.label
            btn = BookToggleButton(text=button_text,book=item, size_hint_y=None, height=30)
            btn.bind(on_release=self.update_toggle_category)
            self.scroll_layout_category.add_widget(btn)


    # ---Récupération de la donnée Toggle de client ---
    def update_toggle_customer(self, widget):
        if widget.state == 'down':
            self.customer_press.append(widget.customer)
            print(self.customer_press)
        else:
            self.customer_press.remove(widget.customer)
            print(self.customer_press)

    # --- Récuperation de la donnée Toggle de livre ---
    def update_toggle_book(self, widget):
        if widget.state == 'down':
            self.book_press.append(widget.book)
            print(self.book_press)
        else:
            self.book_press.remove(widget.book)
            print(self.book_press)

    # --- Récuperation de la donnée Toggle de author
    def update_toggle_author(self, widget):
        if widget.state == 'down':
            self.author_press.append(widget.book)
            print(self.author_press)
        else:
            self.author_press.remove(widget.book)
            print(self.author_press)

    # --- Récuperation de la donnée Category ---
    def update_toggle_category(self, widget):
        if widget.state == 'down':
            self.category_press.append(widget.book)
            print(self.category_press)
        else:
            self.category_press.remove(widget.book)
            print(self.category_press)

    # --- Récuperation de la donnée Type ---
    def update_toggle_type(self, widget):
        if widget.state == 'down':
            self.type_press.append(widget.book)
            print(self.type_press)
        else:
            self.type_press.remove(widget.book)
            print(self.type_press)

    # --- Clbk_previous pour un retour en arrière ---
    def clbk_previous(self, widget):
        self.controller.previous()

    # ---
    def input_research(self, widget, val):
        list = self.controller.input_research(widget)
        self.controller.update_list(list)

    # ---
    def input_research_book(self, widget, val):
        list = self.controller.input_research_book_name(widget)
        self.controller.update_list_book(list)

    def input_research_author(self, widget, val):
        list = self.controller.input_research_book_author(widget)
        self.controller.update_list_author(list)

    # --- Function d'affichage de graphique en fonction du ToggleButton activé ---
    def show_graph(self, widget):
        if self.buttonnborrowing.state == 'down' and self.buttonsell.state == 'normal':
            graph = BaseGraphBorrow(self.date_start.text, self.date_end.text)
            graph.show(self.book_press, self.customer_press, self.type_press,
                       self.category_press, self.author_press)
        elif self.buttonnborrowing.state == 'down' and self.buttonsell.state == 'down':
            graph = BaseGraphBorrow(self.date_start.text, self.date_end.text)
            graph.show(self.book_press, self.customer_press, self.type_press,
                       self.category_press, self.author_press)
            graph2 = BaseGraphSales(self.date_start.text, self.date_end.text)
            graph2.show(self.book_press, self.customer_press, self.type_press,
                       self.category_press, self.author_press)
        elif self.buttonnborrowing.state == 'normal' and self.buttonsell.state == 'down':
            graph = BaseGraphSales(self.date_start.text, self.date_end.text)
            graph.show(self.book_press, self.customer_press, self.type_press,
                       self.category_press, self.author_press)
        elif self.buttonnborrowing.state == 'normal' and self.buttonsell.state == 'normal':
            bonlabel = GridLayout(rows=1 , cols=1)
            textpopup = Label(text="Vous devez choisir au MOIN un type de transaction : VENTE / EMPRUNT ")
            label = Popup(title=" ERREUR ", content=bonlabel)
            label.size_hint=(.5, .5)
            label.open()
            bonlabel.add_widget(textpopup)





# --- CONTROLLEUR ---
class AddManagementSaleBorrowing:
    def __init__(self, menus):
        self.menus = menus
        self.list_books = []
        self.list_author = []
        self.list_customers = []
        self.list_type = []
        self.list_categories = []
        self.view = ManagementSaleBorrowing(self)
        self.load_book()
        self.load_type()
        self.load_customers()

    # --- Changement de ma liste Boutton Author par rapport a l'input ---
    def update_list_author(self, author_list):
        self.view.scroll_layout_author.clear_widgets()
        for book in author_list:
            button_text = book.author
            btn = BookToggleButton(text=button_text,book=book, size_hint_y=None, height=30)
            self.view.scroll_layout_author.add_widget(btn)

    # ---Changement de ma liste de boutton Client par rapport a l'input ---
    def update_list(self, list):
        self.view.scroll_layout_customer.clear_widgets()
        for customer in list:
            button_text = customer.lastname + ' ' + customer.firstname
            btn = ToggleButton(text=button_text, size_hint_y=None, height=30)
            self.view.scroll_layout_customer.add_widget(btn)

    # ---Changement de ma liste de Boutton Nom par rapport a l'input ---
    def update_list_book(self, book_list):
        self.view.scroll_layout.clear_widgets()
        for book in book_list:
            button_text = book.name
            btn = BookToggleButton(text=button_text, book=book, size_hint_y=None, height=30)
            self.view.scroll_layout.add_widget(btn)

    # ---
    def previous(self):
        self.menus.display_menu(self.menus.menu_main)

    # --- List Livre ---
    def load_book(self):
        list_books = ListObject()
        self.list_books = list_books.load_list_of_books()

    def load_author(self):
        list_author = ListObject()
        self.list_author = list_author.load_list_of_author()

    # --- List Client ---
    def load_customers(self):
        list_customers = ListObject()
        self.list_customers = list_customers.load_list_of_coustomers()

    # --- List de type ---
    def load_type(self):
        list_type = ListObject()
        self.list_type = list_type.load_list_of_type()

    # --- List de Categories ---
    def load_categories(self):
        list_categories = ListObject()
        self.list_categories = list_categories.load_list_of_categories()

    # --- Mis a jour les boutton par rapport a la chaine de character du text input NOM/PRENOM --- #
    def input_research(self, widget):
        input_text = widget.text
        list_research_customer = []
        for customer in self.list_customers:
            lastname = customer.lastname
            firstname = customer.firstname
            if re.match(input_text,lastname) or re.match(input_text,firstname):
                list_research_customer.append(customer)
        return list_research_customer

    # --- Mis a jour les Boutton par rapport a la chaine de character du text input NOM ( du livre ) --- #
    def input_research_book_name(self, widget):
        input_text = widget.text
        list_research_book = []
        for book in self.list_books:
            name = book.name
            if re.match(input_text, name) \
                    or re.match(input_text, name):
                list_research_book.append(book)
        return list_research_book

    # --- Mis a jour les Boutton par rapport a la chaine de character du text input AUTHOR ( du livre ) ---
    def input_research_book_author(self, widget):
        input_text = widget.text
        list_research_book_author = []
        for book in self.list_books:
            author = book.author
            if re.match(input_text, author) \
                    or re.match(input_text, author):
                list_research_book_author.append(book)
        return list_research_book_author

    # --- Mis a jour les Boutton par rapport a la chaine de character du text input GENRE ( du livre ) ---
    def input_research_book_genre(self, widget):
        input_text = widget.text
        list_research_book_genre = []
        for book in self.list_books:
            type = book.type
            if re.match(input_text, type) \
                    or re.match(input_text, type):
                list_research_book_genre.append(book)
        return list_research_book_genre

    # --- Mis a jour les Boutton par rapport a la chaine de character du text input CATEGORY ( du livre ) ---
    def input_research_book_type(self, widget):
        input_text = widget.text
        list_research_book_type = []
        for book in self.list_books:
            category = book.category
            if re.match(input_text, category) \
                    or re.match(input_text, category):
                list_research_book_type.append(book)
        return list_research_book_type




