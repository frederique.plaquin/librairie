from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button

class MainMenu(GridLayout):
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, cols=3, rows=3, **kwargs)
        self.controller = controller
        self.sell_books = Button(text='', background_normal='./Public/images/ventesdown.png',
                                 background_down='./Public/images/ventes.png')
        self.sell_books.bind(on_release=self.cblk_menu_buy)
        self.borrow_books = Button(text='', background_normal='./Public/images/pret.png',
                                   background_down='./Public/images/pretdown.png')
        self.borrow_books.bind(on_release=self.cblk_menu_borrow)
        self.books_library = Button(text='', background_normal='./Public/images/bibliotheque.png',
                                    background_down='./Public/images/bibliothequedown.png')
        self.books_library.bind(on_release=self.cblk_menu_library)
        self.sale_borrowing_management = Button(text='',
                                                background_normal='./Public/images/statistiques.png',
                                                background_down='./Public/images/statistiquesdown.png')
        self.sale_borrowing_management.bind(on_release=self.cblk_load_management_sale_borrowing)
        self.customer_management = Button(text='',
                                          background_normal='./Public/images/clients.png',
                                          background_down='./Public/images/clientsdown.png')
        self.customer_management.bind(on_release=self.cblk_load_customer_management_menu)
        self.role_management = Button(text='',
                                      background_normal='./Public/images/personnel.png',
                                      background_down='./Public/images/personneldown.png')
        self.role_management.bind(on_release=self.cblk_load_role_management_menu)

        self.layout()

    def layout(self):
        self.clear_widgets()
        self.add_widget(self.sell_books)
        self.add_widget(self.borrow_books)
        self.add_widget(self.books_library)
        self.add_widget(self.customer_management)
        self.add_widget(self.sale_borrowing_management)

    def clbk_role_management_menu(self, employee):
        if employee.role == "Manager":
            self.remove_widget(self.role_management)
            self.add_widget(self.role_management)

    def cblk_menu_borrow(self, widget):
        self.controller.menu_borrow()

    def cblk_menu_buy(self, widget):
        self.controller.menu_buy()

    def cblk_menu_library(self, widget):
        self.controller.menu_library()

    def cblk_load_customer_management_menu(self, widget):
        self.controller.load_customer_menu()

    def cblk_load_management_sale_borrowing(self, widget):
        self.controller.load_management_sale_borrowing()

    def cblk_load_role_management_menu(self, widget):
        self.controller.load_role_management_menu()

class MainMenuController:
    def __init__(self, menus):
        self.menus = menus
        self.role_management = None
        self.view = MainMenu(self)

    def menu_borrow(self):
        self.menus.display_menu(self.menus.menu_borrow)

    def load_customer_menu(self):
        self.menus.menu_customer_management.preload_list_customer()
        self.menus.display_menu(self.menus.menu_customer_management)

    def menu_buy(self):
        self.menus.display_menu(self.menus.menu_buy)

    def menu_library(self):
        self.menus.display_menu(self.menus.menu_library)


    def load_management_sale_borrowing(self):
        self.menus.display_menu(self.menus.managementsaleborrowing)

    def load_role_management_menu(self):
        self.menus.menu_role_management.preload_list_employees()
        self.menus.display_menu(self.menus.menu_role_management)

    def role_management_menu(self, employee):
        self.view.clear_widgets()
        self.view.layout()
        self.view.clbk_role_management_menu(employee)