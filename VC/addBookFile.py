from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from UI.UI import MyButton
from UI.UI import CustomerButton
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.filechooser import FileChooser
from Model.book import Book
import json


class AddBookFile(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        # MAIN LABEL
        main_label = Label(text="Ajouter un livre à la bibliothèque")
        main_label.pos_hint = {'x': .0, 'y': .42}
        self.add_widget(main_label)
        # BACKGROUND 1
        btn_background1 = Button(size_hint_y=.69, size_hint_x=.61, disabled=True)
        btn_background1.pos_hint = {'x': .055, 'y': .195}
        self.add_widget(btn_background1)
        # LABEL BOOK PRICE
        label_price = Label(text="Prix: ",size_hint_y=.08, size_hint_x=.1)
        label_price.pos_hint = {'x': .06, 'y': .8}
        self.add_widget(label_price)
        # LABEL BOOK ID
        label_book_id = Button(text="ID:", size_hint_y=.08, size_hint_x=.6, disabled=True)
        label_book_id.pos_hint = {'x': .06, 'y': .8}
        self.add_widget(label_book_id)
        # LABEL BOOK AUTHOR
        label_author = Button(text="Auteur:", size_hint_y=.08, size_hint_x=.18, disabled=True)
        label_author.pos_hint = {'x': .24, 'y': .72}
        self.add_widget(label_author)
        # LABEL BOOK TITLE
        label_title = Button(text="Titre:", size_hint_y=.08, size_hint_x=.18, disabled=True)
        label_title.pos_hint = {'x': .24, 'y': .64}
        self.add_widget(label_title)
        # LABEL BOOK TYPE
        label_type = Button(text="Type:", size_hint_y=.08, size_hint_x=.18, disabled=True)
        label_type.pos_hint = {'x': .24, 'y': .56}
        self.add_widget(label_type)
        # LABEL BOOK GENRE
        label_genre = Button(text="Genre:", size_hint_y=.08, size_hint_x=.18, disabled=True)
        label_genre.pos_hint = {'x': .24, 'y': .48}
        self.add_widget(label_genre)
        # LABEL BOOK SYNOPSIS
        label_synopsis = Button(text="Résumé:", size_hint_y=.28, size_hint_x=.18, disabled=True)
        label_synopsis.pos_hint = {'x': .24, 'y': .20}
        self.add_widget(label_synopsis)
        # LABEL BOOK BARCODE IMAGE
        image_code = Button(text="Code barre", size_hint_y=.08, size_hint_x=.18, disabled=True)
        image_code.pos_hint = {'x': .06, 'y': .30}
        self.add_widget(image_code)
        # LABEL BOOK COVER IMAGE
        image_cover = Button(text="Couverture", size_hint_y=.08, size_hint_x=.18, disabled=True)
        image_cover.pos_hint = {'x': .06, 'y': .72}
        self.add_widget(image_cover)
        ########################
        #      TEXT INPUTS     #
        ########################
        self.id = TextInput(text=self.controller.set_book_id(), size_hint_y=.06, size_hint_x=.06, disabled=True)
        self.id.pos_hint = {'x': .43, 'y': .81}
        self.name = TextInput(size_hint_y=.06, size_hint_x=.23)
        self.name.pos_hint = {'x': .43, 'y': .73}
        self.author = TextInput(size_hint_y=.06, size_hint_x=.23)
        self.author.pos_hint = {'x': .43, 'y': .65}
        self.genre = TextInput(size_hint_y=.06, size_hint_x=.23)
        self.genre.pos_hint = {'x': .43, 'y': .57}
        self.type = TextInput(size_hint_y=.06, size_hint_x=.23)
        self.type.pos_hint = {'x': .43, 'y': .49}
        self.price = TextInput(size_hint_y=.06, size_hint_x=.06)
        self.price.pos_hint = {'x': .14, 'y': .81}
        self.synopsis = TextInput(size_hint_y=.26, size_hint_x=.23)
        self.synopsis.pos_hint = {'x': .43, 'y': .21}
        self.barcode = FileChooser(size_hint_y=.06, size_hint_x=.23)
        self.barcode.pos_hint = {'x': .24, 'y': .20}
        self.cover = FileChooser(size_hint_y=.06, size_hint_x=.23)
        self.cover.pos_hint = {'x': .24, 'y': .20}
        ########################
        #        FOOTER        #
        ########################
        # BUTTON RETURN
        btn_return = Button(text="Retour", size_hint_y=.1, size_hint_x=.2)
        btn_return.pos_hint = {'x': .04, 'y': .02}
        btn_return.bind(on_release=self.clbk_return)
        self.add_widget(btn_return)
        # BUTTON CONFIRM BOOK
        btn_modify = Button(text="Confirmer", size_hint_y=.1, size_hint_x=.2)
        btn_modify.pos_hint = {'x': .76, 'y': .02}
        btn_modify.bind(on_release=self.clbk_to_confirm)
        self.add_widget(btn_modify)
        # CHARGE ALL WIDGET ONTO SELF
        self.layout()

    def layout(self):
        self.add_widget(self.id)
        self.add_widget(self.name)
        self.add_widget(self.author)
        self.add_widget(self.genre)
        self.add_widget(self.type)
        self.add_widget(self.price)
        self.add_widget(self.synopsis)
        self.add_widget(self.barcode)
        self.add_widget(self.cover)

    def clbk_return(self, widget):
        self.controller.menu_return()

    def clbk_to_confirm(self, widget):
        self.controller.confirm()


class AddBookFileController:
    def __init__(self, menus):
        self.menus = menus
        self.view = AddBookFile(self)

    def menu_return(self):
        self.menus.display_menu(self.menus.menu_library)

    def confirm(self):
        id = self.set_book_id()
        name = self.view.name.text
        author = self.view.author.text
        price = self.view.price.text
        type = self.view.price.text
        genre = self.view.genre.text
        abstract = self.view.synopsis.text
        # prepare data for saving
        book = Book(id, "", author, name, type, price, genre, abstract, True, [],[])
        book.save_book()
        self.menus.researchBook.load_books()
        self.menus.researchBook.view.list_book_button()
        self.menus.menu_library.load_books()
        self.menus.menu_library.view.list_book_button()
        self.menus.display_menu(self.menus.menu_library)

    def set_book_id(self):
        file_object = open('./Datas/books.json', "r")
        json_content = file_object.read()
        users_list = json.loads(json_content)
        nb_users = len(users_list['books'])
        book_id = str(nb_users + 1)
        file_object.close()
        return book_id

    def display_pop_up_confirmation(self):
        pass

