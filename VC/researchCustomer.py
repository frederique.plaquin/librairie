from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from Model.list import ListObject
from kivy.uix.textinput import TextInput
from UI.UI import *
import re


class ResearchCustomer(FloatLayout):
    def __init__(self, controller, **kwargs):
        super().__init__()
        self.controller = controller
        self.controller.load_customers()
        self.research_car = None
        ########################
        #     TITLE LAYOUT     #
        ########################
        # MAIN LABEL
        self.mainLabel = Label(text="Recherche de client", size_hint_y=1, size_hint_x=1)
        self.mainLabel.pos_hint = {'x': 0, 'y': .42}
        ########################
        # CUSTOMER FILE LAYOUT #
        ########################
        # BACKGROUND
        customer_file = Button(size_hint_y=.6, size_hint_x=.3, disabled=True)
        customer_file.pos_hint = {'x': .68, 'y': 0}
        self.add_widget(customer_file)
        # LASTNAME LABEL
        self.customer_lastname = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.customer_lastname.pos_hint = {'x': .68, 'y': .695}
        # FIRSTNAME LABEL
        self.customer_firstname = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.customer_firstname.pos_hint = {'x': .68, 'y': .590}
        # ADDRESS LABEL
        self.customer_address = Button(size_hint_y=.175, size_hint_x=.296, disabled=True)
        self.customer_address.pos_hint = {'x': .68, 'y': .2}
        # POSTAL CODE LABEL
        self.customer_postal_code = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.customer_postal_code.pos_hint = {'x': .68, 'y': .380}
        # CITY LABEL
        self.customer_city = Button(size_hint_y=.1, size_hint_x=.296, disabled=True)
        self.customer_city.pos_hint = {'x': .68, 'y': .485}
        ########################
        #   RESEARCH LAYOUT    #
        ########################
        # BACKGROUND
        # RESEARCH BOOK LABEL
        self.research_customer_label = Label(text="Rechercher un client", size_hint_y=1, size_hint_x=1)
        self.research_customer_label.pos_hint = {'x': -.35, 'y': .2}
        # RESEARCH BOOK INPUT
        self.research_customer_input = TextInput(size_hint_y=.058, size_hint_x=.2)
        self.research_customer_input.pos_hint = {'x': .05, 'y': .61}
        self.research_customer_input.bind(text=self.input_research)
        ########################
        # SCROLL_VIEW LAYOUT   #
        ########################
        # SCROLL_VIEW LABEL
        self.label_scroll = Label(text="Liste de client:", size_hint_y=1, size_hint_x=1)
        self.label_scroll.pos_hint = {'x': -.13, 'y': .32}
        # SCROLL_VIEW LAYOUT + WIDGET
        self.scroll_layout = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout.bind(minimum_height=self.scroll_layout.setter('height'))
        # BUTTON IN SCROLL_VIEW
        self.scroll_view = ScrollView(size_hint=(0.37, .6), size=(Window.width, Window.height))
        self.scroll_view.pos_hint = {'x': .3, 'y': .2}
        self.scroll_view.add_widget(self.scroll_layout)
        self.list_customer_button()
        ########################
        #        FOOTER        #
        ########################
        # BUTTON CANCEL
        self.btn_cancel = Button(text="Retour", size_hint_y=.07, size_hint_x=.25)
        self.btn_cancel.pos_hint = {'x': 0.02, 'y': 0.02}
        self.btn_cancel.bind(on_release=self.cblk_previous)
        # BUTTON ACCEPT / TERMINATE
        self.btn_accept = Button(text="Confirmer", size_hint_y=.07, size_hint_x=.25)
        self.btn_accept.pos_hint = {'x': .73, 'y': 0.02}
        self.btn_accept.bind(on_release=self.confirm_customer)
        ########################

        # METHOD CALLING ALL DISPLAY
        self.layout()

    # INITIALISATION METHOD FOR ALL WIDGETS FROM THE CLASS LAYOUT
    def layout(self):
        self.clear_widgets()
        self.add_widget(self.mainLabel)
        self.add_widget(self.label_scroll)
        self.add_widget(self.scroll_view)
        self.add_widget(self.btn_accept)
        self.add_widget(self.btn_cancel)
        self.add_widget(self.research_customer_label)
        self.add_widget(self.research_customer_input)
        self.add_widget(self.customer_lastname)
        self.add_widget(self.customer_firstname)
        self.add_widget(self.customer_postal_code)
        self.add_widget(self.customer_city)
        self.add_widget(self.customer_address)

    # CREATION OF BUTTON IN SCROLL_LAYOUT FOR ALL CUSTOMERS CHARGED
    def list_customer_button(self):
        for person in self.controller.list_customers:
            person_data = person.lastname + " " + person.firstname
            btn = CustomerToggleButton(customer=person, text=person_data, size_hint_y=None, height=30)
            btn.bind(on_press=self.on_press_customer)
            self.scroll_layout.add_widget(btn)

    def update_customer_file(self, widget):
        self.customer_firstname.text = str("Prenom : ") + widget.customer.firstname
        self.customer_lastname.text = str("Nom : ") + widget.customer.lastname
        self.customer_city.text = str("Ville : ") + widget.customer.city
        self.customer_postal_code.text = str("Code Postal : ") + widget.customer.zipcode
        self.customer_address.text = str("Adress : ") + widget.customer.address

    # ACTION ON_PRESS BUTTON FROM SCROLL_LAYOUT
    def on_press_customer(self, widget):
        self.update_toggle(widget)
        self.update_customer_file(widget)

    # ACTION ON TEXT_INPUT FROM RESEARCH LAYOUT
    def input_research(self, widget, val):
        list_customers = self.controller.input_research(widget)
        self.update_list(list_customers)

    # UPDATE BUTTON IN SCROLL_VIEW FROM INPUT IN RESEARCH LAYOUT
    def update_list(self, list_customers):
        self.scroll_layout.clear_widgets()
        for person in list_customers:
            person_data = person.name+" "+person.lastname
            btn = CustomerToggleButton(customer=person, text=person_data, size_hint_y=None, height=30)
            self.scroll_layout.add_widget(btn)

    # CHECK IF ONLY ONE BUTTON IS TOGGLE ON
    def update_toggle(self, widget):
        for button in self.scroll_layout.children:
            if widget.customer != button.customer and button.state == "down":
                button.state = "normal"

    # RETURN TO CONTROLLER
    def cblk_previous(self, widget):
        self.controller.previous()

    def confirm_customer(self, widget):
        customer = self.book_checked()
        self.controller.move_book_to_menu(customer)

    def book_checked(self):
        for button in self.scroll_layout.children:
            if button.state == "down":
                return button.customer


class ResearchCustomerController:
    def __init__(self, menus):
        self.menus = menus
        self.previous_page = ""
        self.list_customers = []
        self.view = ResearchCustomer(self)

    def load_customers(self):
        listcustomers = ListObject()
        self.list_customers = listcustomers.load_list_of_coustomers()

    def input_research(self, widget):
        input_text = widget.text
        list_research = []
        for btn in self.list_customers:
            lastname = btn.lastname
            firstname = btn.firstname
            if re.match(input_text, lastname) \
                    or re.match(input_text, firstname)\
                    or re.match(input_text, lastname+" "+firstname) \
                    or re.match(input_text, firstname+" "+lastname):
                list_research.append(btn)
        return list_research

    def finish(self):
        if self.previous_page == "menuBuy":
            self.menus.display_menu(self.menus.menu_buy)
        elif self.previous_page == "menuBorrow":
            self.menus.display_menu(self.menus.menu_borrow)

    def previous(self):
        if self.previous_page == "menuBuy":
            self.menus.display_menu(self.menus.menu_buy)
        elif self.previous_page == "menuBorrow":
            self.menus.display_menu(self.menus.menu_borrow)

    def previouspage(self, pageID):
        self.previous_page = pageID

    def move_book_to_menu(self, customer):
        if self.previous_page == "menuBuy":
            self.menus.menu_buy.charge_customer(customer)
            self.menus.display_menu(self.menus.menu_buy)
        elif self.previous_page == "menuBorrow":
            self.menus.menu_borrow.charge_customer(customer)
            self.menus.display_menu(self.menus.menu_borrow)
