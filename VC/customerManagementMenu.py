from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.scrollview import ScrollView
import kivy.utils as utils
from UI.UI import CustomerButton
from UI.UI import BackgroundCustomerManagement
from Model.list import ListObject
import re


class CustomerManagementMenuView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        self.label_total_customers = ""

        # BACKGROUND MENU
        self.menu_background = BackgroundCustomerManagement(text="", size_hint=(1, 1))
        # BOXLAYOUT BUTTONS BACKGROUND
        self.boxlayout_buttons_background = Button(disabled=True, size_hint=(0.65, 0.112), size_hint_max_y=70,
                                                   pos_hint={'x': 0.17, 'y': 0.8},
                                                   background_normal="",
                                                   background_color=utils.get_color_from_hex('#000000'))
        # BACKGROUND SCROLLVIEW
        self.scrollview_background = Button(disabled=True, size_hint=(0.65, 0.74), pos_hint={'x': 0.17, 'y': 0.05},
                                            background_normal="", background_color=utils.get_color_from_hex('#000000'))
        # BACKGROUND TOTAL BUTTON
        self.label_total_customers_background = Button(text="", size_hint=(0.16, 0.07), disabled=True,
                                                       background_color=utils.get_color_from_hex('#000000'))
        self.label_total_customers_background.pos_hint = {'x': 0.834, 'y': 0.9}

        self.grid1 = GridLayout(cols=1, rows=2)
        self.grid1.size_hint = (0.65, 0.74)
        self.grid1.pos_hint = {'x': 0.17, 'y': 0.05}
        self.grid1_grid_content = BoxLayout(orientation='vertical')
        self.grid_buttons = GridLayout(cols=1, rows=3)
        self.grid_buttons.size_hint_y = (1, 0.3)
        self.button_add_customer = Button(text="AJOUTER UN CLIENT", background_normal="",
                                           background_color=utils.get_color_from_hex('#93124d'))
        self.button_add_customer.bind(on_release=self.cblk_load_add_customer_file)
        self.button_add_customer.size_hint = (0.5, 1)

        # RESEARCH BOOK LABEL
        self.research_book_label = Button(text="CHERCHER UN CLIENT", disabled=True, background_normal="",
                                          background_color=utils.get_color_from_hex('#000000'))
        self.research_book_label.size_hint = (0.5, 1)
        # RESEARCH BOOK INPUT
        self.research_book_input = TextInput(multiline=False, size_hint_y=1, size_hint_x=.5)
        self.research_book_input.bind(text=self.cblk_input_research)

        self.grid_customer_list_scrollview = ScrollView(size_hint=(1, 0.8))
        self.grid_customer_list = GridLayout(cols=3, size_hint_y=None)
        self.grid_customer_list.bind(minimum_height=self.grid_customer_list.setter('height'))

        self.button_return = Button(text="RETOUR", size_hint=(0.15, 0.07), background_normal="",
                                           background_color=utils.get_color_from_hex('#93124d'))
        self.button_return.pos_hint = {'x': 0.011, 'y': 0.05}
        self.button_return.bind(on_release=self.cblk_navigate_to_main_menu)

        self.controller.load_customers()
        self.layout()

    def layout(self):
        self.clear_widgets()
        self.add_widget(self.menu_background)
        self.add_widget(self.boxlayout_buttons_background)
        self.add_widget(self.scrollview_background)
        self.add_widget(self.label_total_customers_background)
        self.add_widget(self.grid1)
        self.grid1.add_widget(self.grid1_grid_content)
        self.grid1_grid_content.add_widget(self.grid_buttons)
        self.grid_buttons.add_widget(self.button_add_customer)
        self.grid_buttons.add_widget(self.research_book_label)
        self.grid_buttons.add_widget(self.research_book_input)
        self.grid1_grid_content.add_widget(self.grid_customer_list_scrollview)
        self.add_widget(self.button_return)

    def cblk_input_research(self, widget, val):
        list_customers = self.controller.input_research(widget)
        self.display_customer_list(list_customers)

    def display_customer_list(self, customer_list):
        self.grid_customer_list.clear_widgets()
        self.grid_customer_list_scrollview.clear_widgets()
        if self.label_total_customers:
            self.remove_widget(self.label_total_customers)

        self.label_total_customers = Button(text="Total Clients : " + str(len(customer_list)),
                                            size_hint=(0.16, 0.07), disabled=True)
        self.label_total_customers.pos_hint = {'x': 0.834, 'y': 0.9}

        self.add_widget(self.label_total_customers)
        self.grid_customer_list_scrollview.add_widget(self.grid_customer_list)

        if customer_list:
            book_list = customer_list
        else:
            book_list = self.controller.list_customers

        for item in book_list:
            customer = item
            customer_lastname = customer.lastname
            customer_firstname = customer.firstname

            button_text = customer_lastname + ' ' + customer_firstname
            label_customer = Button(text=button_text, disabled=True)
            label_customer.size_hint = (0.6, None)
            label_customer.height = 40
            button_show_customer_file = CustomerButton(text="Fiche client", customer=customer, background_normal="",
                                           background_color=utils.get_color_from_hex('#991457'))
            button_show_customer_file.size_hint = (0.2, None)
            button_show_customer_file.height = 40
            button_show_customer_file.bind(on_release=self.cblk_navigate_to_customer_file)
            button_modify_customer_file = CustomerButton(text="Modifier", customer=customer, background_normal="",
                                           background_color=utils.get_color_from_hex('#93124d'))
            button_modify_customer_file.size_hint = (0.2, None)
            button_modify_customer_file.height = 40
            button_modify_customer_file.bind(on_release=self.cblk_modify_customer_file)
            self.grid_customer_list.add_widget(label_customer)
            self.grid_customer_list.add_widget(button_show_customer_file)
            self.grid_customer_list.add_widget(button_modify_customer_file)

    def cblk_load_add_customer_file(self, widget):
        self.controller.navigate_to_add_customer_file()

    def cblk_navigate_to_customer_file(self, widget):
        self.controller.navigate_to_customer_file(widget)

    def cblk_modify_customer_file(self, widget):
        self.controller.navigate_to_modify_customer_file(widget)

    def cblk_navigate_to_main_menu(self, widget):
        self.controller.navigate_to_main_menu()


class CustomerManagementMenuController:
    def __init__(self, menus):
        self.menus = menus
        self.list_customers = []
        self.view = CustomerManagementMenuView(self)

    def load_customers(self):
        list_object = ListObject()
        self.list_customers = list_object.load_list_of_coustomers()

    def navigate_to_add_customer_file(self):
        self.menus.file_add_customer.view.preload_form()
        self.menus.display_menu(self.menus.file_add_customer)

    def navigate_to_customer_file(self, widget):
        customer = widget.customer
        self.menus.file_customer.preload_customer(customer)
        self.menus.display_menu(self.menus.file_customer)

    def navigate_to_modify_customer_file(self, widget):
        customer = widget.customer
        self.menus.file_customer_modify.preload_customer(customer)
        self.menus.display_menu(self.menus.file_customer_modify)

    def navigate_to_main_menu(self):
        self.menus.display_menu(self.menus.menu_main)

    def preload_list_customer(self):
        self.load_customers()
        self.view.display_customer_list(self.list_customers)

    def input_research(self, widget):
        input_text = widget.text
        list_research = []
        for btn in self.list_customers:
            lastname = btn.lastname
            firstname = btn.firstname
            if re.match(input_text, lastname) \
                    or re.match(input_text, firstname)\
                    or re.match(input_text, lastname+" "+firstname) \
                    or re.match(input_text, firstname+" "+lastname):
                list_research.append(btn)
        return list_research
