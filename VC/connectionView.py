from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout
from UI.UI import MyButton
from Model.Employee import Employee


class MenuConnectionView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        self.label_popup = MyButton(text="La connexion a échoué !", disabled=True, background_color=[0.15, 0.7, 1, 1],
                                    valign="center", halign="center")

        # CONNECTION SCREEN BACKGROUND
        self.connection_screen_background = MyButton(text="", disabled=True)
        self.connection_screen_background.size_hint = (0.5, 0.5)
        self.connection_screen_background.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        # MAIN CONTAINER
        self.main_container = GridLayout(cols=1, rows=3)
        self.main_container.size_hint = (0.5, 0.5)
        self.main_container.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        # MAIN CONTENT
        # SCREEN TITLE
        self.screen_title = MyButton(text="BIENVENUE SUR LIBAPP", size_hint=(1, 1), disabled=True, valign="center",
                                     halign="center", font_size=25)
        # FORM CONTAINER
        self.form_container = GridLayout(cols=2, rows=4)
        # LABEL ID
        self.label_id = MyButton(text="Identifiant :", size_hint=(1, 1), disabled=True, valign="center", halign="right",
                                 padding=(10, 0), font_size=17)
        self.label_id.size_hint = (0.35, 1)
        # INPUT ID
        self.input_id = TextInput(multiline=False, text="", size_hint=(1, 1))
        self.input_id.size_hint = (1.65, 1)
        # LABEL PASSWORD
        self.label_password = MyButton(text="Mot de passe :", size_hint=(1, 1), disabled=True, valign="center",
                                       halign="right", padding=(10, 0), font_size=17)
        # INPUT PASSWORD
        self.input_password = TextInput(multiline=False, text="", size_hint=(1, 1), password=True)
        # BUTTON LOGIN
        self.button_login = MyButton(text="LOGIN", size_hint=(1, 1), valign="center", halign="center", padding=(10, 0),
                                     font_size=20)
        self.button_login.bind(on_release=self.clbk_do_login)
        self.layout()

    def layout(self):
        self.clear_widgets()
        self.add_widget(self.connection_screen_background)
        self.add_widget(self.main_container)
        self.main_container.add_widget(self.screen_title)
        self.main_container.add_widget(self.form_container)
        self.form_container.add_widget(self.label_id)
        self.form_container.add_widget(self.input_id)
        self.form_container.add_widget(self.label_password)
        self.form_container.add_widget(self.input_password)
        self.main_container.add_widget(self.button_login)
        '''self.add_widget(self.label_mail)
        self.add_widget(self.input_mail)
        self.add_widget(self.label_passwd)
        self.add_widget(self.input_passwd)
        self.add_widget(self.label_confirm_passwd)
        self.add_widget(self.input_confirm_passwd)
        self.add_widget(self.button_connection)'''

    def clbk_do_login(self, widget):
        self.controller.do_login()

    def clbk_menu_images(self, widget):
        self.controller.navigate_menu_map()

    def connection(self, widget):
        #self.controller.enregistrer_utilisateur(self.input_mail.text)
        pass

class MenuConnectionController:
    def __init__(self, menus):
        self.menus = menus
        self.view = MenuConnectionView(self)
        self.user_id = ''

    def navigate_menu_map(self):
        self.menus.afficher_menu(self.menus.menu_map)

    def do_login(self):
        username = self.view.input_id.text
        password = self.view.input_password.text

        employee_id = Employee.employee_login(self, username, password)

        if employee_id:
            employee = Employee.get_employee(self, employee_id)
            self.menus.header.login(employee)
            self.menus.menu_main.role_management_menu(employee)
            self.menus.display_menu(self.menus.menu_main)
        else:
            self.view.add_widget(self.view.label_popup)
            Clock.schedule_once(self.cblk_reload_form, 2)

    def cblk_reload_form(self, dt):
        self.view.remove_widget(self.view.label_popup)