from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from UI.UI import EmployeeButton
from UI.UI import BackgroundRoleManagement
from kivy.uix.scrollview import ScrollView
from Model.list import ListObject
import kivy.utils as utils
import re


class RoleManagementMenuView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller
        self.label_total_employees = ""

        # BACKGROUND MENU
        self.menu_background = BackgroundRoleManagement(text="", size_hint=(1, 1))
        # BOXLAYOUT BUTTONS BACKGROUND
        self.boxlayout_buttons_background = Button(disabled=True, size_hint=(0.65, 0.112), size_hint_max_y=70,
                                                   pos_hint={'x': 0.17, 'y': 0.8}, background_normal="",
                                                   background_color=utils.get_color_from_hex('#000000'))
        # BACKGROUND SCROLLVIEW
        self.scrollview_background = Button(disabled=True, size_hint=(0.65, 0.74), pos_hint={'x': 0.17, 'y': 0.05},
                                            background_normal="", background_color=utils.get_color_from_hex('#000000'))
        # BACKGROUND TOTAL BUTTON
        self.label_total_customers_background = Button(text="", size_hint=(0.16, 0.07), disabled=True,
                                                       background_color=utils.get_color_from_hex('#000000'))
        self.label_total_customers_background.pos_hint = {'x': 0.834, 'y': 0.9}

        self.grid1 = GridLayout(cols=1, rows=2)
        self.grid1.size_hint = (0.65, 0.74)
        self.grid1.pos_hint = {'x': 0.17, 'y': 0.05}
        self.grid1_grid_content = BoxLayout(orientation='vertical')
        self.grid_buttons = GridLayout(cols=1, rows=3)
        self.grid_buttons.size_hint_y = (1, 0.3)
        self.button_add_employee = Button(text="AJOUTER UN EMPLOYE", background_normal="",
                                          background_color=utils.get_color_from_hex('#196e93'))
        self.button_add_employee.bind(on_release=self.cblk_load_add_employee_file)
        self.button_add_employee.size_hint = (0.5, 1)
        # RESEARCH BOOK LABEL
        self.research_book_label = Button(text="CHERCHER UN EMPLOYE", disabled=True, background_normal="",
                                          background_color=utils.get_color_from_hex('#000000'))
        self.research_book_label.size_hint = (0.5, 1)
        # RESEARCH BOOK INPUT
        self.research_book_input = TextInput(multiline=False, size_hint_y=1, size_hint_x=.5)
        self.research_book_input.bind(text=self.cblk_input_research)

        self.grid_employee_list_scrollview = ScrollView(size_hint=(1, 0.8))
        self.grid_employee_list = GridLayout(cols=3, size_hint_y=None)
        self.grid_employee_list.bind(minimum_height=self.grid_employee_list.setter('height'))

        self.button_return = Button(text="RETOUR", size_hint=(0.15, 0.07), background_normal="",
                                          background_color=utils.get_color_from_hex('#196e93'))
        self.button_return.pos_hint = {'x': 0.011, 'y': 0.05}
        self.button_return.bind(on_release=self.cblk_navigate_to_main_menu)

        self.controller.load_employees()
        self.layout1()

    def layout1(self):
        self.clear_widgets()
        self.add_widget(self.menu_background)
        self.add_widget(self.boxlayout_buttons_background)
        self.add_widget(self.scrollview_background)
        self.add_widget(self.label_total_customers_background)
        self.add_widget(self.grid1)
        self.grid1.add_widget(self.grid1_grid_content)
        self.grid1_grid_content.add_widget(self.grid_buttons)
        self.grid_buttons.add_widget(self.button_add_employee)
        self.grid_buttons.add_widget(self.research_book_label)
        self.grid_buttons.add_widget(self.research_book_input)
        self.grid1_grid_content.add_widget(self.grid_employee_list_scrollview)
        self.add_widget(self.button_return)

    def cblk_input_research(self, widget, val):
        list_employees = self.controller.input_research(widget)
        self.display_employees_list(list_employees)

    def display_employees_list(self, list_employees):
        self.grid_employee_list.clear_widgets()
        self.grid_employee_list_scrollview.clear_widgets()

        self.label_total_employees = Button(text="Nb Employés : " + str(len(list_employees)),
                                            size_hint=(0.16, 0.07), disabled=True)
        self.label_total_employees.pos_hint = {'x': 0.834, 'y': 0.9}

        self.add_widget(self.label_total_employees)
        self.grid_employee_list_scrollview.add_widget(self.grid_employee_list)

        if list_employees:
            list_employees = list_employees
        else:
            list_employees = self.controller.list_employees

        for item in list_employees:
            employee = item
            employee_lastname = employee.lastname
            employee_firstname = employee.firstname

            button_text = employee_lastname + ' ' + employee_firstname
            label_employee = Button(text=button_text, disabled=True)
            label_employee.size_hint = (0.84, None)
            label_employee.height = 40
            button_show_employee_file = EmployeeButton(text="", employee=employee,
                                                       background_normal="./Public/images/employeefilesm.png",
                                                       background_down="./Public/images/employeefiledownsm.png")
            button_show_employee_file.size_hint = (0.08, 1)
            button_show_employee_file.height = 40
            button_show_employee_file.width = 40
            button_show_employee_file.bind(on_release=self.cblk_navigate_to_employee_file)
            button_modify_employee_file = EmployeeButton(text="", employee=employee,
                                                         background_normal="./Public/images/modifyfileemployee.png",
                                                         background_down="./Public/images/modifyfileemployeedown.png")
            button_modify_employee_file.size_hint = (0.08, None)
            button_modify_employee_file.height = 40
            button_modify_employee_file.bind(on_release=self.cblk_modify_employee_file)
            self.grid_employee_list.add_widget(label_employee)
            self.grid_employee_list.add_widget(button_show_employee_file)
            self.grid_employee_list.add_widget(button_modify_employee_file)

    def cblk_load_add_employee_file(self, widget):
        self.controller.navigate_to_add_employee_file()

    def cblk_navigate_to_employee_file(self, widget):
        self.controller.navigate_to_employee_file(widget)

    def cblk_modify_employee_file(self, widget):
        self.controller.navigate_to_modify_employee_file(widget)

    def cblk_navigate_to_main_menu(self, widget):
        self.controller.navigate_to_main_menu()


class RoleManagementMenuController:
    def __init__(self, menus):
        self.menus = menus
        self.list_employees = []
        self.view = RoleManagementMenuView(self)

    def load_employees(self):
        list_object = ListObject()
        self.list_employees = list_object.load_list_employees()

    def navigate_to_add_employee_file(self):
        self.menus.file_add_employee.view.preload_form()
        self.menus.display_menu(self.menus.file_add_employee)

    def navigate_to_employee_file(self, widget):
        employee = widget.employee
        self.menus.file_employee.preload_employee(employee)
        self.menus.display_menu(self.menus.file_employee)

    def navigate_to_modify_employee_file(self, widget):
        employee = widget.employee
        self.menus.file_employee_modify.preload_employee(employee)
        self.menus.display_menu(self.menus.file_employee_modify)

    def navigate_to_main_menu(self):
        self.menus.display_menu(self.menus.menu_main)

    def preload_list_employees(self):
        self.load_employees()
        self.view.display_employees_list(self.list_employees)

    def input_research(self, widget):
        input_text = widget.text
        list_research = []
        for btn in self.list_employees:
            lastname = btn.lastname
            firstname = btn.firstname
            if re.match(input_text, lastname) \
                    or re.match(input_text, firstname)\
                    or re.match(input_text, lastname+" "+firstname) \
                    or re.match(input_text, firstname+" "+lastname):
                list_research.append(btn)
        return list_research

