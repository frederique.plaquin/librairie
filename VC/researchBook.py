from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from kivy.uix.textinput import TextInput
from Model.list import ListObject
from UI.UI import *
import re


class ResearchBook(FloatLayout):
    def __init__(self, controller, **kwargs):
        super().__init__()
        self.controller = controller
        self.controller.load_books()
        self.research_car = None
        ########################
        #     TITLE LAYOUT     #
        ########################
        # MAIN LABEL
        self.mainLabel = Label(text="Recherche de livre", size_hint_y=1, size_hint_x=1)
        self.mainLabel.pos_hint = {'x': 0, 'y': .42}
        ########################
        #   BOOK FILE LAYOUT   #
        ########################
        # BOOK FILE BACKGROUND
        self.book_file_background = Button(disabled=True, size_hint_y=.6, size_hint_x=.3)
        self.book_file_background.pos_hint = {'x': .68, 'y': .2}
        # BOOK TITLE
        self.book_title = Button(disabled=True, size_hint_y=.08, size_hint_x=.296)
        self.book_title.pos_hint = {'x': .682, 'y': .715}
        # BOOK AUTHOR
        self.book_author = Button(disabled=True, size_hint_y=.08, size_hint_x=.296)
        self.book_author.pos_hint = {'x': .682, 'y': .63}
        # BOOK PRICE
        self.book_price = Button(disabled=True, size_hint_y=.08, size_hint_x=.296)
        self.book_price.pos_hint = {'x': .682, 'y': .545}
        # BOOK SYNOPSIS
        self.book_synopsis = Button(halign='justify', disabled=True, size_hint_y=.26, size_hint_x=.296)
        self.book_synopsis.pos_hint = {'x': .682, 'y': .28}
        self.book_synopsis_text = Label(size_hint=(1, 1))
        self.book_synopsis_text.size_hint = (.2, .24)
        self.book_synopsis_text.pos_hint = {'x': .19, 'y': -.22}
        # BOOK AVAILABILITY
        self.book_availability = Button(disabled=True, size_hint_y=.06, size_hint_x=.20)
        self.book_availability.pos_hint = {'x': .73, 'y': .21}
        ########################
        #    RESEARCH LAYOUT   #
        ########################
        # RESEARCH BOOK LABEL
        self.research_book_label = Label(text="Rechercher un livre", size_hint_y=1, size_hint_x=1)
        self.research_book_label.pos_hint = {'x': -.35, 'y': .2}
        # RESEARCH BOOK INPUT
        self.research_book_input = TextInput(size_hint_y=.058, size_hint_x=.2)
        self.research_book_input.pos_hint = {'x': .05, 'y': .61}
        self.research_book_input.bind(text=self.input_research)
        ########################
        # SCROLL_VIEW LAYOUT   #
        ########################
        # SCROLL_VIEW LAYOUT + WIDGET
        self.scroll_layout = GridLayout(cols=1, spacing=1, size_hint_y=None)
        self.scroll_layout.bind(minimum_height=self.scroll_layout.setter('height'))
        # BUTTON IN SCROLL_VIEW
        self.scroll_view = ScrollView(size_hint=(0.37, .6), size=(Window.width, Window.height))
        self.scroll_view.pos_hint = {'x': .3, 'y': .2}
        self.scroll_view.add_widget(self.scroll_layout)
        self.list_book_button()
        ########################
        #        FOOTER        #
        ########################
        # BUTTON CANCEL
        self.btn_cancel = Button(text="Retour", size_hint_y=.07, size_hint_x=.25)
        self.btn_cancel.pos_hint = {'x': 0.02, 'y': 0.02}
        self.btn_cancel.bind(on_release=self.cblk_previous)
        # BUTTON CONFIRM
        self.btn_confirm = Button(text="Confirmer", size_hint_y=.07, size_hint_x=.25)
        self.btn_confirm.pos_hint = {'x': .73, 'y': .02}
        self.btn_confirm.bind(on_release=self.confirm_book)


        # METHOD CALLING ALL DISPLAY
        self.layout()

    # INITIALISATION METHOD FOR ALL WIDGETS FROM THE CLASS LAYOUT
    def layout(self):
        self.clear_widgets()
        self.add_widget(self.mainLabel)
        self.add_widget(self.book_file_background)
        self.add_widget(self.book_title)
        self.add_widget(self.book_author)
        self.add_widget(self.book_price)
        self.add_widget(self.book_synopsis)
        self.add_widget(self.book_availability)
        self.add_widget(self.scroll_view)
        self.add_widget(self.btn_cancel)
        self.add_widget(self.research_book_label)
        self.add_widget(self.research_book_input)
        self.add_widget(self.book_synopsis_text)
        self.add_widget(self.btn_confirm)

    def list_book_button(self):
        self.scroll_layout.clear_widgets()
        for book in self.controller.list_books:
            button_text = book.author + " " + book.name
            btn = BookToggleButton(text=button_text, book=book, size_hint_y=None, height=30)
            btn.bind(on_press=self.on_press_book)
            self.scroll_layout.add_widget(btn)

    def confirm_book(self, widget):
        book = self.book_checked()
        self.controller.move_book_to_menu(book)

    def input_research(self, widget, input_text):
        list_book = self.controller.input_research(widget)
        self.update_list(list_book)

    def update_book_file(self, widget):
        self.book_title.text = str("Nom : ")+widget.book.name
        self.book_author.text = str("Auteur : ")+widget.book.author
        self.book_price.text = str("Prix : ")+widget.book.price + str(" €")
#       self.book_synopsis.text = widget.book.abstract
        self.book_availability.text = widget.book.r_availability()

    def update_list(self, book_list):
        self.scroll_layout.clear_widgets()
        for book in book_list:
            button_text = book.author + " " + book.name
            btn = BookToggleButton(text=button_text, book=book, size_hint_y=None, height=30)
            btn.bind(on_press=self.on_press_book)
            self.scroll_layout.add_widget(btn)

    def on_press_book(self, widget):
        self.update_toggle(widget)
        self.update_book_file(widget)

    def update_toggle(self, widget):
        for button in self.scroll_layout.children:
            if widget.book != button.book and button.state == "down":
                button.state = "normal"

    def cblk_previous(self, widget):
        self.controller.previous()

    def book_checked(self):
        for button in self.scroll_layout.children:
            if button.state == "down":
                return button.book


class ResearchBookController:
    def __init__(self, menus):
        self.menus = menus
        self.previous_page = "menuBorrow"
        self.list_books = []
        self.view = ResearchBook(self)

    def load_books(self):
        listbooks = ListObject()
        self.list_books = listbooks.load_list_of_books()

    def input_research(self, widget):
        input_text = widget.text
        list_research = []
        for book in self.list_books:
            author = book.author
            name = book.name
            if re.match(input_text, author) \
                    or re.match(input_text, name)\
                    or re.match(input_text, author+" "+name) \
                    or re.match(input_text, name+" "+author):
                list_research.append(book)
        return list_research

    def previous(self):
        if self.previous_page == "menuBuy":
            self.menus.display_menu(self.menus.menu_buy)
        elif self.previous_page == "menuBorrow":
            self.menus.display_menu(self.menus.menu_borrow)

    def previouspage(self, page_id):
        self.previous_page = page_id

    def move_book_to_menu(self, book):
        if self.previous_page == "menuBuy":
            self.menus.menu_buy.charge_data(book)
            self.menus.display_menu(self.menus.menu_buy)
        elif self.previous_page == "menuBorrow":
            self.menus.menu_borrow.charge_data(book)
            self.menus.display_menu(self.menus.menu_borrow)
