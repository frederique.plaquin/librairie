from Model.person import Person
import json
import hashlib
import os


class Employee(Person):
    def __init__(self, employee_id, lastname, name, mail, role, username):
        Person.__init__(self, lastname, name, mail)
        self.id = employee_id
        self.lastname = lastname
        self.firstname = name
        self.mail = mail
        self.role = self.get_role(role)
        self.username = username
        self.password = ""

    def get_role(self, role):
        if role:
            role = "Manager"
        else:
            role = "Vendeur"

        return role

    def set_role(self, role):
        if role == "manager":
            role = True
        else:
            role = False

        return role

    def set_employee_id(self, path):
        file_object = open(path, "r")
        json_content = file_object.read()
        users_list = json.loads(json_content)
        nb_users = len(users_list['employees'])
        user_id = str(nb_users + 1)
        file_object.close()

        return user_id

    def add_employee_to_register(self, path, employee_id, lastname, firstname, mail, role, password):
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['employees']

            # préparation des données de la catégorie
            new_data = {
                "id": employee_id,
                "lastname": lastname,
                "firstname": firstname,
                "mail": mail,
                "role": role,
                "username": firstname + ' ' + lastname
            }

            temp.append(new_data)

            # saving as json
            try:
                Employee.save_as_json(self, path, data)
                Employee.save_employee_login(self, employee_id, firstname, lastname, password)
            except:
                pass

    def save_employee_login(self, employee_id, firstname, lastname, password):
        employee_login = firstname + ' ' + lastname
        key = password
        # path
        path = './Datas/database.json'
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['users']

            # préparation des données de la catégorie
            new_data = {
                "id": employee_id,
                "username": employee_login,
                "key": key
            }

            temp.append(new_data)

            # saving as json
            try:
                Employee.save_as_json(self, path, data)
            except:
                pass

    def hash_password(self, salt, employee_password):
        key = hashlib.pbkdf2_hmac('sha256', employee_password.encode('utf-8'), salt, 100000)
        return key

    def modify_employee(self, widget):
        employee = widget.employee
        # opening json file
        path = './Datas/employees.json'
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['employees']

            for item in temp:
                if item['id'] == employee.id:
                    item['id'] = employee.id
                    item['lastname'] = employee.lastname
                    item['firstname'] = employee.firstname
                    item['mail'] = employee.mail
                    item['role'] = employee.role
                    item['username'] = employee.username

            try:
                Employee.save_as_json(self, path, data)
                Employee.modify_employee_login(self, employee.id, employee.firstname, employee.lastname,
                                               employee.password)
            except:
                pass

    def modify_employee_login(self, employee_id, firstname, lastname, password):
        employee_login = firstname + ' ' + lastname
        key = password
        # path
        path = './Datas/database.json'
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['users']

            for item in temp:
                if item['id'] == employee_id:
                    item['id'] = employee_id
                    item['username'] = employee_login
                    item['key'] = key

            # saving as json
            try:
                Employee.save_as_json(self, path, data)
            except:
                pass

    def delete_employee(self, path, employee_id):
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_employees = data['employees']
            temp = []

            for item in list_employees:
                if item['id'] != employee_id:
                    temp.append(item)

        # json_object
        json_object = {
                "employees": temp
            }

        return json_object

    def delete_employee_login(self, path, employee_id):
        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_employees = data['users']
            temp = []

            for item in list_employees:
                if item['id'] != employee_id:
                    temp.append(item)

        # json_object
        json_object = {
                "users": temp
            }

        return json_object

    def save_as_json(self, path, data):
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4)

    def employee_login(self, sent_username, sent_password):
        # path
        path = './Datas/database.json'

        # opening json file
        with open(path, 'rb') as file:
            data = json.load(file)
            list_users = data['users']

            for item in list_users:
                username = item['username']
                key = item['key']

                if username == sent_username and key == key:
                    employee_id = item['id']
                    return employee_id

    def get_employee(self, employee_id):
        # path
        path = './Datas/employees.json'

        with open(path, 'rb') as file:
            data = json.load(file)
            list_employees = data['employees']

        for item in list_employees:
            if item['id'] == employee_id:
                employee_lastname = item['lastname']
                employee_firstname = item['firstname']
                employee_mail = item['mail']
                employee_role = item['role']
                employee_username = item['username']

                employee = Employee(employee_id, employee_lastname, employee_firstname, employee_mail, employee_role,
                                    employee_username)

                return employee

    def create_book(self):
        pass

    def archive_book(self):
        pass

    def modify_book(self):
        pass

