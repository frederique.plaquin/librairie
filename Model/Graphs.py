import matplotlib.pyplot as plt
from Model.list import ListObject
from Model.Borrow import Borrow
from datetime import datetime, timedelta
import time
import random


class BaseGraphBorrow:

    def __init__(self, start_date, end_date):
        self.title = "Achats"
        self.x_label = "Mois de l' année"
        self.y_label = "Nombre d'achats"
        self.start_date_str = start_date
        self.end_date_str = end_date
        self.show_grid = True


    def show(self, list_book, list_customer, list_type, list_category, list_author):

        plt.subplot(211)
        plt.grid(self.show_grid)
        plt.title("Titre")


        for book in list_book:

            list = ListObject()
            borrows_list = list.nbr_borrows(book)
            borrows = []

            # start_date_str = self.start_date_str
            start_date_str = "25/01/2021"
            #transformation de la date
            start_date_datetime = datetime.strptime(start_date_str, "%d/%m/%Y")
            # start_date_date = time.mktime(datetime.strptime(start_date_str, "%d/%m/%Y").timetuple())
            # end_date_str = self.end_date_str
            end_date_str = '05/05/2022'
            end_date_datetime = datetime.strptime(end_date_str, "%d/%m/%Y")
            # end_date_date = time.mktime(datetime.strptime(end_date_str, "%d/%m/%Y").timetuple())

            # date incrementation de 4 semaines
            temp_date = start_date_datetime
            interval = timedelta(weeks=4)

            while temp_date <= end_date_datetime:
                interval_borrows = []
                i = 0

                start_interval = temp_date
                end_interval = temp_date + interval

                for borrow in borrows_list:
                    borrow_start_date_str = borrow['start_date']
                    borrow_start_date_time = datetime.strptime(borrow_start_date_str, "%d/%m/%Y")

                    if borrow_start_date_time >= start_interval and borrow_start_date_time <= end_interval:
                        i += 1
                        interval_borrows.append(borrow['id'])

                borrows.append(i)

                temp_date += interval

            # affect a random color to plot
            x = [0, 1, 2]
            y = [0, 1, 2]
            r = random.random()
            b = random.random()
            g = random.random()
            color = (r, g, b)

            plt.plot(borrows, "b", marker=".", label=book.id+' - '+book.name, c=color)

        plt.ylabel(self.y_label)
        plt.xlabel(self.x_label)
        plt.legend()

        plt.show()


class BaseGraphSales:

    def __init__(self, start_date, end_date):

        self.start_date_str = start_date
        self.end_date_str = end_date
        self.show_grid = True

    def show(self, list_book, list_customer, list_type, list_category, list_author):
        title = "Ventes"
        x_label = "Mois de l' année"
        y_label = "Nombre d' emprunts"
        plt.subplot(211)
        plt.grid(self.show_grid)
        plt.title("Titre")

        for book in list_book:

            list = ListObject()
            sales_list = list.nbr_sales(book)
            sales = []

            # start_date_str = self.start_date_str
            start_date_str = "25/01/2021"
            # transformation de la date
            start_date_datetime = datetime.strptime(start_date_str, "%d/%m/%Y")
            # start_date_date = time.mktime(datetime.strptime(start_date_str, "%d/%m/%Y").timetuple())
            # end_date_str = self.end_date_str
            end_date_str = '05/05/2022'
            end_date_datetime = datetime.strptime(end_date_str, "%d/%m/%Y")
            # end_date_date = time.mktime(datetime.strptime(end_date_str, "%d/%m/%Y").timetuple())

            # date incrementation de 4 semaines
            temp_date = start_date_datetime
            interval = timedelta(weeks=4)

            while temp_date <= end_date_datetime:
                interval_sales = []
                i = 0

                start_interval = temp_date
                end_interval = temp_date + interval

                for sale in sales_list:
                    sale_start_date_str = sale['transaction_date']
                    sale_start_date_time = datetime.strptime(sale_start_date_str, "%d/%m/%Y")

                    if sale_start_date_time >= start_interval and sale_start_date_time <= end_interval:
                        i +=1
                        interval_sales.append(sale['id'])

                sales.append(i)

                temp_date += interval

            x = [0, 1, 2]
            y = [0, 1, 2]
            r = random.random()
            b = random.random()
            g = random.random()
            color = (r, g, b)

            plt.plot(sales, "b", marker=".", label=book.id+' - '+book.name, c=color)

        plt.ylabel(y_label)
        plt.xlabel(x_label)
        plt.legend()


        title = "Ventes"
        x_label = "Mois de l' année"
        y_label = "Nombre de ventes"
        plt.subplot(212)
        plt.grid(self.show_grid)
        plt.title(title)

        ##########################
        for author in list_author:

            list = ListObject()
            sales_list = list.nbr_sales_by_author(author)
            sales = []

            # start_date_str = self.start_date_str
            start_date_str = "25/01/2021"
            # transformation de la date
            start_date_datetime = datetime.strptime(start_date_str, "%d/%m/%Y")
            # start_date_date = time.mktime(datetime.strptime(start_date_str, "%d/%m/%Y").timetuple())
            # end_date_str = self.end_date_str
            end_date_str = '05/05/2022'
            end_date_datetime = datetime.strptime(end_date_str, "%d/%m/%Y")
            # end_date_date = time.mktime(datetime.strptime(end_date_str, "%d/%m/%Y").timetuple())

            # date incrementation de 4 semaines
            temp_date = start_date_datetime
            interval = timedelta(weeks=4)

            while temp_date <= end_date_datetime:
                interval_sales = []
                i = 0

                start_interval = temp_date
                end_interval = temp_date + interval

                for sale in sales_list:
                    sale_start_date_str = sale['transaction_date']
                    sale_start_date_time = datetime.strptime(sale_start_date_str, "%d/%m/%Y")

                    if sale_start_date_time >= start_interval and sale_start_date_time <= end_interval:
                        i += 1
                        interval_sales.append(sale['id'])

                sales.append(i)

                temp_date += interval

            x = [0, 1, 2]
            y = [0, 1, 2]
            r = random.random()
            b = random.random()
            g = random.random()
            color = (r, g, b)

            plt.plot(sales, "b", marker=".", label=author, c=color)
            print('####')
            print(list_author)
            print('####')

        plt.ylabel(y_label)
        plt.xlabel(x_label)
        plt.legend()

        title = "Ventes"
        x_label = "Mois de l' année"
        y_label = "Nombre de ventes"
        plt.subplot(212)
        plt.grid(self.show_grid)
        plt.title(title)


        ########################
        for author in list_author:

            list = ListObject()
            sales_list = list.nbr_sales_by_author(author)
            sales = []

            # start_date_str = self.start_date_str
            start_date_str = "25/01/2021"
            # transformation de la date
            start_date_datetime = datetime.strptime(start_date_str, "%d/%m/%Y")
            # start_date_date = time.mktime(datetime.strptime(start_date_str, "%d/%m/%Y").timetuple())
            # end_date_str = self.end_date_str
            end_date_str = '05/05/2022'
            end_date_datetime = datetime.strptime(end_date_str, "%d/%m/%Y")
            # end_date_date = time.mktime(datetime.strptime(end_date_str, "%d/%m/%Y").timetuple())

            # date incrementation de 4 semaines
            temp_date = start_date_datetime
            interval = timedelta(weeks=4)

            while temp_date <= end_date_datetime:
                interval_sales = []
                i = 0

                start_interval = temp_date
                end_interval = temp_date + interval

                for sale in sales_list:
                    sale_start_date_str = sale['transaction_date']
                    sale_start_date_time = datetime.strptime(sale_start_date_str, "%d/%m/%Y")

                    if sale_start_date_time >= start_interval and sale_start_date_time <= end_interval:
                        i += 1
                        interval_sales.append(sale['id'])

                sales.append(i)

                temp_date += interval

            x = [0, 1, 2]
            y = [0, 1, 2]
            r = random.random()
            b = random.random()
            g = random.random()
            color = (r, g, b)

            plt.plot(sales, "b", marker=".", label=author, c=color)
            print('####')
            print(list_author)
            print('####')

        plt.ylabel(y_label)
        plt.xlabel(x_label)
        plt.legend()

        plt.show()


