import json
import os

class Borrow:
    def __init__(self, id=None, list_books=[], start_date="", end_date="", client_id=""):
        self.id = id
        self.list_book_id = list_books
        self.start_date = start_date
        self.end_date = end_date
        self.client_id = client_id
        self.list_books = []

    def save_borrow(self):
        path = './Datas/borrows_history.json'
        with open(path, 'rb') as file:
            data = json.load(file)
            temp = data['borrows_history']
            # préparation des données de la catégorie
            new_data = {
                "id": self.id,
                "start_date":self.start_date,
                "end_date": self.end_date,
                "list_book_id": self.list_book_id,
                "customer_id": self.client_id,
            }
            temp.append(new_data)
        self.save_as_json(data)

    def set_borrows_history_id(self):
        path = './Datas/borrows_history.json'
        file_object = open(path, "r")
        json_content = file_object.read()
        borrows_history_list = json.loads(json_content)
        nb_users = len(borrows_history_list['borrows_history'])
        user_id = str(nb_users + 1)
        file_object.close()
        return user_id

    def save_as_json(self, data):
        path = './Datas/borrows_history.json'
        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=4, default=str)

